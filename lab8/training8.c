/*
 * FEEG6002 Laboratory 8 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-28 12:00 - 14:00
 */

#include <stdio.h>
#include <stdlib.h>



int string_length(char s[]) {
  int i = 0;
  while (s[i] != '\0') {
    ++i;
  }

  return i;
}



char* mix(char *s1, char *s2) {
  int i;
  int s_len = string_length(s1); /* length of source arrays */
  int r_len = 2*s_len; /* length of new array */
  char *r;

  r = (char *) malloc(r_len*sizeof(char));

  if (r == NULL) {
    return NULL;
  }

  /*printf("Length of string s: %i\n", s_len);
  printf("Size of r: %i\n", r_len*sizeof(char));
  printf("Length of string r: %i\n", r_len);*/
  for (i = 0; i < s_len; i++) {
    r[2*i]   = s1[i];
    r[2*i+1] = s2[i];
  }

  /* assign end of string character */
  r[r_len] = '\0';
  /*r[r_len] = s1[s_len];*/

  return r;
}



void use_mix(void) {
  /* Sample FEEG6002 code to test the 'mix' function */

  char s1[] = "Hello World";
  char s2[] = "1234567890!";

  printf("s1 = %s\n", s1);
  printf("s2 = %s\n", s2);
  /*printf("Length of string s1: %i\n", sizeof s1);*/
  printf("r  = %s\n", mix(s1, s2));
}



int main(void) {
  /*use_mix();*/

  return 0;
}
