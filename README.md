FEEG6002 Advanced Computational Methods 1
=========================================

All labs are done in C.

Lab 1
-----

* Training: Write a Hello World program
* Lab: Experiment with printf



Lab 2
-----

* Training: Fahrenheit/Celsius table
* Lab: Computing interest



Lab 3
-----

* Training: Tabulate sin(x)
* Lab: Tabulate sin(x) and cos(x)



Lab 4
-----

* Training: Generate a number larger than the factorial of n
* Lab: Compute factorials with fallbacks for invalid inputs



Lab 5
-----

* Training: The length of a string
* Lab: Reversing a string (=char array)



Lab 6
-----

* Training: Strip whitespace from the right
* Lab: Strip whitespace from the left



Lab 7
-----

* Training 1: Determine pi using trapezoidal integration
* Training 2: Allocating an array of longs dynamically
* Lab: Creating an array of Fibonacci numbers



Lab 8
-----

* Training: Merge two strings together with alternating characters
* Lab: Implement bubble sorting



lab 9
-----

* Lab: Optimise Python computation of Mandelbrot Set by converting into Cython