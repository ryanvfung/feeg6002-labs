/*
 * FEEG6002 Laboratory 2 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-18 12:00 - 14:00
 * Tabulate values of sine and cosine
 */

#include <stdio.h>
#include <math.h>

#define N 10
#define XMIN 1
#define XMAX 10

int main(void) {
  int i;
  double x = 0;
  double sin_x = 0;
  double cos_x = 0;
  for (i=0; i<N; i++) {
    x = XMIN + (XMAX - XMIN) / (N - 1.) * i;
    sin_x = sin(x);
    cos_x = cos(x);
    printf("%f %f %f\n", x, sin_x, cos_x);
  }

  return 0;
}
