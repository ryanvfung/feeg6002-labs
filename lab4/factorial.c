/*
 * FEEG6002 Laboratory 4 Training & Lab
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-01 12:00 - 14:00
 */

#include <stdio.h>
#include <limits.h>   /* variable limits for integers */
#include <math.h>

long maxlong(void) {
  return LONG_MAX;
}

double upper_bound(long n) {
  return n < 6 ? 719 : pow(n/2., n);
}

long factorial(long n) {
  if (n < 0) {
    return -2;
  } else if (upper_bound(n) >= LONG_MAX) {
    return -1;
  } else {
    long j;
    long result = 1;
    for (j = 2; j <= n; j++) {
      result *= j;
    }
    return result;
  }
}

int main(void) {
  long i;

  /* The next line should compile once "maxlong" is defined. */
  printf("maxlong() = %ld\n\n", maxlong());

  /* The next code block should compile once "upper_bound" is defined. */

  for (i = 0; i < 10; i++) {
    printf("upper_bound(%2ld) = %g\n", i, upper_bound(i));
  }

  for (i = -1; i < 20; i++) {
    printf("factorial(%2ld) = %ld\n", i, factorial(i));
  }

  return 0;
}
