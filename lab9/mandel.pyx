def mandel_cy(long n, long itermax=100, double xmin=-2, double xmax=0.5, double ymin=-1.25, double ymax=1.25):
    '''
	Edited by Ryan Fung
	
    Mandelbrot fractal computation using Python.

    (n, n) are the output image dimensions
    itermax is the maximum number of iterations to do.
    xmin, xmax, ymin, ymax specify the region of the
    set to compute.

    Returns a list of lists of ints, representing the image matrix with
    n rows and n columns.
    '''

    # create list containing n lists, each containing n zeros
    # (i.e. a matrix, represented as a list of lists)
    its = [ [0] * n for i in xrange(n)]
    # The data in the matrix are iterations, so 'its' is the plural of
    # IT for ITeration.


    # iterate through all matrix elements
    ##from math import sqrt # using python sqrt
    cdef:
        # double x, y
        double x, y, c_real, c_imag, z_real, z_imag, z_real_new, z_imag_new
        long it, ix, iy
    
    for ix in xrange(0, n):
        for iy in xrange(0, n):
            # compute the position (x, y) corresponding to matrix element
            x = xmin + ix * (xmax - xmin) / n
            y = ymin + iy * (ymax - ymin) / n
            # Need to count iterations
            it = 0
            # c is the complex number with the given
            # x, y coordinates in the complex plane, i.e. c = x + i * y
            # where i = sqrt(-1)
            ##c = x + y * 1j # original python
            ##z = 0 # original python
            #? separate complex number into real and imaginary parts
            c_real = x
            c_imag = y
            z_real = 0
            z_imag = 0
            # Here is the actual Mandelbrot criterion: we update z to be
            # z <- z^2 + c until |z| <= 2. We could the number of iterations
            # required. This number of iterations is the data we need to compute
            # (and plot if desired).
            ## while it < itermax and abs(z) < 2.0: # original python
            ## while it < itermax and sqrt(z_real*z_real+z_imag*z_imag) < 2.0:
            #? explicitly writing out absolute operation and squaring both sides
            while it < itermax and ((z_real*z_real)+(z_imag*z_imag)) < 4.0:
                ##z = z ** 2 + c
                #? refactor as separated real and imaginary parts into temp vars
                z_real_new = (z_real*z_real) - (z_imag*z_imag) + c_real
                z_imag_new = (2 * z_real * z_imag) + c_imag
                #? revert back from temp vars to orig vars
                z_real = z_real_new
                z_imag = z_imag_new
                it += 1

            #print("ix={}, iy={}, x={}, y={}, c={}, z={}, abs(z)={}, it={}"
            #    .format(ix, iy, x, y, c, z, abs(z), it))

            # Store the result in the matrix
            its[ix][iy] = it

    return its

