/*
 * FEEG6002 Laboratory 1 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-11 12:00 - 14:00
 */

#include <stdio.h>

int main(void) {
  printf("Hello World\n");

  return 0;
}
