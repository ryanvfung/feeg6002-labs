/*
 * FEEG6002 Laboratory 1 Lab (part 1)
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-11 12:00 - 14:00
 */

#include <stdio.h>

int main(void) {
  int i = 42;
  float x = 3.10;
  printf("i=%i\nx=%4.2f\n", i, x);

  return 0;
}
