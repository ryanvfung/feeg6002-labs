/*
 * FEEG6002 Laboratory 1 Lab (part 2)
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-11 12:00 - 14:00
 */

#include <stdio.h>

int main(void) {
  int i;

  /*
  Following statement should assign the number of characters outputted to
  stdout from printf to the variable i.
  Reference: http://devdocs.io/c/io/fprintf#Return_value
  */
  i = (int) printf("What am I doing?\n");

  /*
  Testing code to check return value of i
  */
  printf("%i\n", i);

  return 0;
}
