/*
 * FEEG6002 Laboratory 7
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-22 12:00 - 14:00
 */

#include <stdio.h>
#include <stdlib.h>



long* make_long_array(long n) {
  long *array;

  array = (long *) malloc(n*sizeof(long));

  if (array == NULL) {
    printf("Memory allocation failed");
  }
  return array;

  /* explicitly return NULL if memory allocation fails */
  /*if (array == NULL) {
    printf("Memory allocation failed");
    return NULL;
  } else {
    return array;
  }*/
}



long* make_fib_array(long n) {
  /* Return an array of Fibonacci numbers up to n */
  int i;
  long *fib_array = make_long_array(n);

  /* Check memory has been unsuccessfully allocated */
  if (fib_array == NULL) { return fib_array; }

  /* Assign values for when n <= 2 */
  if (n <= 0) { return NULL; }
  if (n >= 1) { fib_array[0] = (long) 0; }
  if (n >= 2) { fib_array[1] = (long) 1; }

  /* Recursively compute Fibonacci values */
  for (i=2; i<=n; i++) {
    fib_array[i] = fib_array[i-1] + fib_array[i-2];
  }

  return fib_array;
}



void use_fib_array(long N) {
  /* N is the maximum number for fibarray length */
  long n;      /* counter for fibarray length */
  long i;      /* counter for printing all elements of fibarray */
  long *fibarray;  /* pointer to long -- pointer to the fibarray itself*/

  /* Print one line for each fibarray length n*/
  for (n=2; n<=N; n++) {
    /* Obtain an array of longs with data */
    fibarray = make_fib_array(n);

    /* Print all elements in array */
    printf("fib(%2ld) : [",n);
    for (i=0; i<n; i++) {
      printf(" %ld", fibarray[i]);
    }
    printf(" ]\n");

    /* free array memory */
    free(fibarray);
  }
}



int main(void) {
  /*use_fib_array(10);*/


  /* Test if memory allocation returns expected results */
  /*long *fibarray;

  fibarray = make_fib_array(1000000000);
  if(fibarray==NULL){
    printf("\nfibarray is NULL\n");
  } else {
    printf("\nfibarray is fine\n");
  }*/

  return 0;
}
