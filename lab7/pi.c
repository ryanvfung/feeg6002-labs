/*
 * FEEG6002 Laboratory 7 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-22 12:00 - 14:00
 */

#include<stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf("%8.5f seconds used.\n", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */


double f(double x) {
  return sqrt(1-x*x);
}

double pi(long n) {
  int a = -1;
  int b = 1;
  int i = 1;
  double x;
  double h = (b-a)/(double) n;
  double s = 0.5 * f(a) + 0.5 * f(b);

  for (i=1; i<=n-1; i++) {
    x = a + i*h;
    s = s + f(x);
  }
  return s * h * 2;
}

int main(void) {
  /* Declarations */

  /* Code */
  START;               /* Timing measurement starts here */
  /* Code to be written by student, calling functions from here is fine
     if desired
  */

  printf("Pi: %f\n", pi(10000000));

  STOP;                /* Timing measurement stops here */
  PRINTTIME;           /* Print timing results */


  return 0;
}
