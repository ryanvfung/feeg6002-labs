/*
 * FEEG6002 Laboratory 2 Lab
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-18 12:00 - 14:00
 */

#include <stdio.h>

int main(void) {
  double s = 1000;    /* sum borrowed, in GBP */
  double debt = s;    /* actual debt */
  double rate = 0.03; /* interest rate, i.e. 3% */
  double interest;    /* interest acquired in a particular month */
  double total_interest = 0; /* total interest paid over all months */
  double frac;               /* percentage of interest paid to sum borrowed */
  int month;
  for (month = 1; month <= 24; month++) { /* loop over 24 months */
    interest = debt * rate;  /* interest in this month */
    total_interest += interest;
    debt += interest;
    frac = total_interest/s*100; /* converted to percentage */
    printf("month %2d: debt=%7.2f", month, debt);
    printf(", interest=%5.2f", interest);
    printf(", total_interest=%7.2f", total_interest);
    printf(", frac=%6.2f%%\n", frac);
  }

  return 0;
}
