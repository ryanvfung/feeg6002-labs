/*
 * FEEG6002 Laboratory 2 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-10-18 12:00 - 14:00
 */

#include <stdio.h>

int main(void) {
  int temp_c;       /* Temperature in degrees Celsius */
  double temp_f;    /* Temperature in degrees Fahreinheit */

  for (temp_c = -30; temp_c <= 30; temp_c += 2) {
    temp_f = temp_c * 9. / 5. + 32.; /* Convert from Celsius to Fahreinheit */
    printf("%3d = %5.1f\n", temp_c, temp_f);
  }

  return 0;
}
