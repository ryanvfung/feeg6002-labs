/*
 * FEEG6002 Laboratory 6 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-15 12:00 - 14:00
 */

#include <stdio.h>

void rstrip(char s[]) {
  int len = 0;  /* Length of string (location of \0) */
  int end;      /* Strip string to this index */

  /* Find end of string identifier */
  while (s[len]!='\0') {
    printf("%c\n", s[len]);
    len++;
  }
  end = len - 1;

  /* Find new end of string */
  while (s[end]==' ' || s[end]=='\t' || s[end]=='\n' || s[end]=='\r') {
    end--;
  }
  end++;

  s[end] = '\0';

  /*printf("String length: %i\n", len);*/
  /*printf("String end: %i\n", end);*/
  /*printf("String end: %i", len);*/
  /*printf("RSTRIP results: #start#%s#end#\n", s);*/
}

int main(void) {
  /*char s[] = "Hello World.   ";*/
  /*rstrip(s);*/
  /*printf("RSTRIP results: #start#%s#end#\n", s);*/

  return 0;
}
