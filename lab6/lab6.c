/*
 * FEEG6002 Laboratory 6 Lab
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-15 12:00 - 14:00
 */

#include <stdio.h>

void lstrip(char s[]) {
  int start = 0; /* Strip string from this index */
  int i; /* Copying index */

  /* Find new start of string */
  while (s[start]==' ' || s[start]=='\t' || s[start]=='\n' || s[start]=='\r') {
    start++;
  }
  i = start;

  /* Copy string by character forwards */
  s[0] = s[i];
  while (s[i++]!='\0') {
    s[i-start] = s[i];
  }

  /*printf("String length: %i\n", len);*/
  /*printf("String end: %i\n", end);*/
  /*printf("String end: %i", len);*/
  /*printf("LSTRIP results: #start#%s#end#\n", s);*/
}

int main(void) {
  /*char s[] = "   Hello World.   ";*/
  /*char s[] = "";*/
  /*char s[] = "  ";*/
  /*lstrip(s);*/
  /*printf("LSTRIP results: #start#%s#end#\n", s);*/

  return 0;
}
