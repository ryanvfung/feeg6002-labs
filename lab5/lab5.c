/*
 * FEEG6002 Laboratory 5
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-08 12:00 - 14:00
 */

#include <stdio.h>

long string_length(char s[]) {
  long i = 0;
  while (s[i] != '\0') {
    ++i;
  }

  return i;
}

void reverse(char source[], char target[]) {
  long j;
  long len = string_length(source);
  /*printf("Source string length: %i\n", len);*/

  for (j = 0; j<=len; j++) {
    target[j] = source[len-j-1];
  }

  /* Set end of string character */
  target[len]='\0';
}

int main(void) {
  /*printf("%i\n", string_length("Hello World"));
  printf("%i\n", string_length("x"));
  printf("%i\n", string_length("line 1\tline 2\n"));*/
  /*char a[13] = "Hello World!";
  char b[13] = "############";*/

  /*reverse(a, b);*/
  /*printf("Source: %s\n", a);
  printf("Target: %s\n", b);*/

  /*char a[13] = "Hello World!";*/
  /*printf("%s\n", a);*/

  return 0;
}
