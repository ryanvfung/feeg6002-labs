/*
 * FEEG6002 Laboratory 5 Training
 * Author: Ryan Fung
 * Student ID: 26230593
 * Class Date: 2016-11-08 12:00 - 14:00
 */

#include <stdio.h>

long string_length(char s[]) {
  long i = 0;
  while (s[i] != '\0') {
    ++i;
  }

  return i;
}

int main(void) {
  /*printf("%i\n", string_length("Hello World"));
  printf("%i\n", string_length("x"));
  printf("%i\n", string_length("line 1\tline 2\n"));*/

  return 0;
}
